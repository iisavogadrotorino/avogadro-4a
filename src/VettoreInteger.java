import java.util.Random;

public class VettoreInteger {
    static int statico1 = 1;

    public static void main(String[] args) {
        Persona persone[];
        persone = new Persona[5];
        Random random = new Random();
        // write your code here
        System.out.println("Vettore Persone");

        for (int i = 0; i < persone.length; i++) {
            persone[i] = new Persona();
            persone[i].cognome = "Rossi";
            persone[i].nome = "Mario";
            persone[i].nato = 1980;
        }

        for (Persona persona : persone) {
            System.out.println("Persona:" + persona);
            System.out.println("cognome:" + persona.cognome);
            System.out.println("nome:" + persona.nome);
            System.out.println("nato:" + persona.nato);
        }
    }
}
